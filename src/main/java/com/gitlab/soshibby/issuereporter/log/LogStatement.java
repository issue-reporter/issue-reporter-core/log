package com.gitlab.soshibby.issuereporter.log;

import java.util.Date;
import java.util.Map;

public class LogStatement {
    private String traceId;
    private String spanId;
    private String parentSpanId;
    private String exceptionSpanId;
    private String stackTrace;
    private Map<String, Object> fields;
    private Date date;

    public LogStatement(String traceId, String spanId, String parentSpanId, String exceptionSpanId, String stackTrace, Map<String, Object> fields, Date date) {
        this.traceId = traceId;
        this.spanId = spanId;
        this.parentSpanId = parentSpanId;
        this.exceptionSpanId = exceptionSpanId;
        this.stackTrace = stackTrace;
        this.fields = fields;
        this.date = date;
    }

    public String getTraceId() {
        return traceId;
    }

    public String getSpanId() {
        return spanId;
    }

    public String getParentSpanId() {
        return parentSpanId;
    }

    public String getExceptionSpanId() {
        return exceptionSpanId;
    }

    public String getStackTrace() {
        return stackTrace;
    }

    public Map<String, Object> getFields() {
        return fields;
    }

    public Date getDate() {
        return date;
    }

    @Override
    public String toString() {
        return "LogStatement{" +
                "traceId='" + traceId + '\'' +
                ", spanId='" + spanId + '\'' +
                ", parentSpanId='" + parentSpanId + '\'' +
                ", exceptionSpanId='" + exceptionSpanId + '\'' +
                ", stackTrace='" + stackTrace + '\'' +
                ", fields=" + fields +
                ", date=" + date +
                '}';
    }
}
